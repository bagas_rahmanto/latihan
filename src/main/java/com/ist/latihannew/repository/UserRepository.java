package com.ist.latihannew.repository;


import com.ist.latihannew.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UserRepository extends JpaRepository<User,Long> {

    List<User> findByEmail(String email);
    List<User> findByUsername(String username);
    List<User> findByEmailAndUsername(String email, String username);

}
