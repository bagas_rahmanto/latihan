package com.ist.latihannew.repository;

import com.ist.latihannew.entity.CustomProduct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

@Service
public class CustomProductDAO {



    @Autowired
    private EntityManager em;


    public List<CustomProduct> getCustomProduct(){
        String nativeQuery="select RAND(100) as id,p.kodeproduct, p.description as nama_product, p.price,pg.name_group, ps.description as sales_desc from product p\n" +
                "INNER JOIN product_group pg on pg.id= p.product_group_id\n" +
                "INNER JOIN product_sales ps on ps.id= p.product_sales_id";
        Query q= em.createNativeQuery(nativeQuery,"CustomProductQueryMap");

        return q.getResultList();
    }
}
