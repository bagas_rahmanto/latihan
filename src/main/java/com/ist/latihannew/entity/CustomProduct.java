package com.ist.latihannew.entity;

import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Data
@SqlResultSetMapping(name="CustomProductQueryMap",entities = {
        @EntityResult(entityClass = CustomProduct.class, fields = {
                @FieldResult(name="id",column = "id"),
                @FieldResult(name="kodeProduct",column = "kodeproduct"),
                @FieldResult(name="namaProduct",column = "nama_ptoduct"),
                @FieldResult(name="price",column = "price"),
                @FieldResult(name="namaGroup",column = "name_group"),
                @FieldResult(name="salesDesc",column = "sales_desc"),
        })
})
public class CustomProduct {
    @Id
    private String id;
    private String kodeProduct;
    private String namaProduct;
    private BigDecimal price;
    private String namaGroup;
    private String salesDesc;
}
