package com.ist.latihannew;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LatihannewApplication {

	public static void main(String[] args) {
		SpringApplication.run(LatihannewApplication.class, args);
	}

}
