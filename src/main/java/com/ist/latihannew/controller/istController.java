package com.ist.latihannew.controller;

import com.ist.latihannew.entity.CustomProduct;
import com.ist.latihannew.entity.User;
import com.ist.latihannew.repository.CustomProductDAO;
import com.ist.latihannew.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Slf4j
public class istController {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private CustomProductDAO dao;

    @GetMapping("/hello")
    public String helloWorld(){
        return "Hello World";
    }


    @GetMapping("/getAllUser")
    public ResponseEntity<List<User>> getAllUser(){
        List<User> result=userRepository.findAll();
        if(result==null|| result.isEmpty()){
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(result,HttpStatus.OK);
    }
    //comment
    @PostMapping("/saveUser")
    public ResponseEntity<User> saveUser(@RequestBody User user){
        try{
            User u =userRepository.save(user);
            return new ResponseEntity<>(u, HttpStatus.CREATED);
        }catch (Exception e){
            log.error("Error saving{}", e.getMessage());
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }
    @DeleteMapping("/deleteUser")
    public ResponseEntity<String> deleteUser(@RequestParam String userId){
        try{
            userRepository.deleteById(Long.valueOf(userId));
            return new ResponseEntity<>("Deleted",HttpStatus.OK);
        }catch (Exception e){
            log.error("Error deleting user{}", e.getMessage());
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }


    @GetMapping("/filterByEmail")
    public ResponseEntity<List<User>> getByEmail(@RequestParam String email){
        List<User> result=userRepository.findByEmail(email);
        if(result==null|| result.isEmpty()){
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(result,HttpStatus.OK);
    }



    @GetMapping("/filterByUsername")
    public ResponseEntity<List<User>> getByUsername(@RequestParam String username){
        List<User> result=userRepository.findByUsername(username);
        if(result==null|| result.isEmpty()){
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(result,HttpStatus.OK);
    }

    @GetMapping("/getByEmailAndUsername")
    public ResponseEntity<List<User>> getByEmailAndUsername(@RequestParam String username, String email){
        List<User> result=userRepository.findByEmailAndUsername(email,username);
        if(result==null|| result.isEmpty()){
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(result,HttpStatus.OK);
    }

    @GetMapping("/getCustomProduct")
    public ResponseEntity<List<CustomProduct>> getCustomProduct(){
        List<CustomProduct> result=dao.getCustomProduct();
        if(result==null|| result.isEmpty()){
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(result,HttpStatus.OK);
    }
}
